FROM ubuntu:24.04

RUN set -ex; \
    apt update; \
    apt install -y --no-install-recommends \
        bash-completion \
        ca-certificates \
        curl \
    ; \
    curl -L https://github.com/okd-project/okd/releases/download/4.15.0-0.okd-2024-03-10-010116/openshift-client-linux-4.15.0-0.okd-2024-03-10-010116.tar.gz | tar xvz -C /usr/local/bin; \
    rm /usr/local/bin/README.md; \
    curl -L https://github.com/okd-project/okd/releases/download/4.15.0-0.okd-2024-03-10-010116/openshift-install-linux-4.15.0-0.okd-2024-03-10-010116.tar.gz | tar xvz -C /usr/local/bin; \
    rm /usr/local/bin/README.md; \
    mkdir /etc/bash_completion.d; \
    oc completion bash > /etc/bash_completion.d/oc_bash_completion; \
    chmod a+r /etc/bash_completion.d/oc_bash_completion; \
    openshift-install completion bash > /etc/bash_completion.d/openshift-install_bash_completion; \
    chmod a+r /etc/bash_completion.d/openshift-install_bash_completion; \
    echo 'source <(kubectl completion bash)' >>~/.bashrc; \
    echo 'complete -o default -F __start_kubectl k' >>~/.bashrc;

CMD ["/usr/local/bin/oc"]